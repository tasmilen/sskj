(function () {
    // 动态插入iconfont
    var css = document.createElement('link');
    css.href = 'https://at.alicdn.com/t/font_927668_9kr7q97dym.css';
    css.rel = 'stylesheet';
    document.head.appendChild(css);
    // ie浏览器提示
    if (!!window.ActiveXObject || 'ActiveXObject' in window) {
        if (location == top.location) {
            document.body.style.display = 'none'
            alert('暂不支持IE浏览器, 请使用现代浏览器并切换到极速模式访问(谷歌, 火狐, 360, qq浏览器等)')
        }
    }
})()
var G = {
    DEBOUNCE_DELAY: 200,
    isMock() {
        return localStorage.getItem('mock') || false;
    },
    to(url) {
        return top.location.hash = url
    },
    toto(url) {
        return window.open(url, '_blank').location;
    },
    paramsSerializer(params) {
        var win = top === window ? window : top
        return win.Qs.stringify(params, {
            arrayFormat: 'brackets'
        })
    },
    toThousands(number) {
        return (number + '').replace(/\B(?=(\d{3})+\b)/g, ',');
    },
    toThousandsRound(num, n) {
        return this.toThousands(this.roundFixed(num, n))
    },
    roundFixed(num, n) {
        if (!num) {
            return 0
        }
        num = num + '';
        n = n || 2;
        var idx = num.indexOf('.'),
            cut = num.charAt(idx + n + 1),
            s_num = num.slice(0, idx + n + 1);

        // 如果没有小数点直接return
        if (~idx) {
            // 如果小数点后n+1位大于5,结果为 (s_num去掉小数点+1)/10的n次方
            if (cut >= 5) {
                num = (s_num.replace(/\./, '') - 0 + 1) / Math.pow(10, n);
            } else {
                num = parseFloat(s_num);
            }
        }
        return num;
    },
    isNumber(val) {
        return toString.call(val) === '[object Number]' && !isNaN(val)
    },
    getFormatTime(timestamp) {
        timestamp = timestamp || 0
        var t = new Date(timestamp),
            Y = t.getYear(),
            M = t.getMonth() + 1,
            D = t.getDate(),
            h = t.getHours(),
            m = t.getMinutes(),
            s = t.getSeconds();
        if (Y < 1900) Y = Y + 1900;
        M = M < 10 ? '0' + M : M;
        D = D < 10 ? '0' + D : D;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        s = s < 10 ? '0' + s : s;
        return Y + '-' + M + '-' + D + ' ' + h + ':' + m + ':' + s;
    },
    getFormatYMD(timestamp) {
        timestamp = timestamp || 0
        var t = new Date(timestamp),
            Y = t.getYear(),
            M = t.getMonth() + 1,
            D = t.getDate();
        if (Y < 1900) Y = Y + 1900;
        M = M < 10 ? '0' + M : M;
        D = D < 10 ? '0' + D : D;
        return Y + '-' + M + '-' + D;
    },
    getFormatYMDhi(timestamp) {
        timestamp = timestamp || 0
        var t = new Date(timestamp),
            Y = t.getYear(),
            M = t.getMonth() + 1,
            D = t.getDate(),
            h = t.getHours(),
            m = t.getMinutes();
        if (Y < 1900) Y = Y + 1900;
        M = M < 10 ? '0' + M : M;
        D = D < 10 ? '0' + D : D;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        return Y + '-' + M + '-' + D + ' ' + h + ':' + m;
    },
    getHourMin(ts) {
        //ts:秒
        var o = {
            "hh": parseInt(ts / (60 * 60)), //时   
            "mm": parseInt((ts % (60 * 60)) / 60), //分   
            "ss": parseInt((ts % (60 * 60)) % 60), //秒   
            "S": ts //总秒
        };
        var temp = o.hh + ':';
        if (o.mm < 10) {
            temp = temp + '0' + o.mm;
        } else {
            temp = temp + o.mm;
        }
        temp += ':'
        if (o.ss < 10) {
            temp = temp + '0' + o.ss;
        } else {
            temp = temp + o.ss;
        }
        return temp;
    },
    
    // 获取转换的时长 如: 1小时30分
    fM(tM) {
        if (!tM) {
            return '0分'
        }
        var temp = ''
        var o = {
            "dd": parseInt(tM / (24 * 60)), //天
            "hh": parseInt((tM % (24 * 60)) / 60), //时   
            "mm": parseInt((tM % (24 * 60)) % 60), //分   
            "M": tM //总分  
        };
        if (o.dd > 0) {
            temp += o.dd + '天 '
        }
        if (o.dd > 0 || o.hh > 0) {
            temp += o.hh + '小时 '
        }
        if (o.dd > 0 || o.hh > 0 || o.mm > 0) {
            temp += o.mm + '分'
        }
        return temp;
    },

    // 文件大小转换
    getfGMK(b) {
        if (b < 1024 * 1024) {
            return G.roundFixed(b / 1024, 2) + ' K';
        } else if (b < 1024 * 1024 * 1024) {
            return G.roundFixed(b / (1024 * 1024), 2) + ' M';
        } else if (b < 1024 * 1024 * 1024 * 1024) {
            return G.roundFixed(b / (1024 * 1024 * 1024), 2) + ' G';
        } else if (b < 1024 * 1024 * 1024 * 1024 * 1024) {
            return G.roundFixed(b / (1024 * 1024 * 1024 * 1024), 2) + ' T';
        } else if (b < 1024 * 1024 * 1024 * 1024 * 1024 * 1024) {
            return G.roundFixed(b / (1024 * 1024 * 1024 * 1024 * 1024), 2) + ' P';
        }
    },

    // 防抖
    debounce: (func, delay) => {
        var timer = null
        return function () {
            timer && clearTimeout(timer)
            timer = setTimeout(() => {
                func.apply(this, arguments)
            }, delay)
        }
    },

    // 节流
    throttle: (func, wait) => {
        var start = 0
        return function () {
            let now = Date.now()
            if (now - start >= wait) {
                func.apply(this, arguments)
                start = now
            }
        }
    },

    vm: {
        resetPwd() {
            top.vm.resetPwd()
        }
    },
}
var H = {
    home: '首页',
    notice: '公告',
    notice_detail: '公告详情',
    buyHash: '购买算力',
    hashOrder: '算力订单',
    hashInfo: '算力信息',
    dataManage: '数据管理',
    costCoreGet: '获取算力列表',
    costCoreUse: '算力使用列表',
    buyHash_hashCacl: '算力订单',
    pay_status: '订单支付状态',
    modelManage: '模型管理',
    ai_create: 'AI_创建训练',
    ai_detailed: 'AI_训练明细',
    ai_training: 'AI_训练',
}
var ipfsConfig = {
    ip: 'localhost',
    aipPort: '5001',
    gatewayPort: '8088',
    protocol: 'http'
}

axios.defaults.withCredentials = true;
// axios.defaults.baseURL = 'http://192.168.1.137:8181'