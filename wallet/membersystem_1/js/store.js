function Store() {
    this.limit = 12;
    this.list = [];
}

Store.prototype.set = function(key, value) {
    var item = this.get(key, true);
    if (!item) {
        var data = {};
        
        if (this.list.length >= this.limit) {
            this.list.pop();
        }
        
        data[key] = value;
        this.list.unshift(data);
    } else {
        item[key] = value;
    }
}

Store.prototype.get = function(key, isReturnItem) {
    var item = null;
    for (var i = 0; i < this.list.length; i++) {
        if (this.list[i].hasOwnProperty(key)) {
            item = this.list.splice(i, 1);
            this.list.unshift(item);
            break;
        }
    }
    
    return isReturnItem ? item : item[key];
}

// 使用方法
// var store = new Store();
// store.set(key, value);           // 存数据
// store.get(key);                  // 取数据