$(document).ready(function(){
    var selectDate;
    var startDate = new Date();
    // 获取当前月
    var curMonth = startDate.getFullYear() + '-' + (startDate.getMonth() + 1);
    console.log('init:', curMonth);
    var baseUrl = 'http://www.dengwenhao.com:727';
    var token = 'TAIC06v6_l7Oe5TNKwuPDIvt000dk2CkTKI_risRiO';
    var localStore = {};    //用于本地缓存获取的数据。
    var isTest = false;
    
    init_view();
    
    function is_show_profit() {
        return $("#checkbox_1").is(':checked');
    }

    function is_show_reward() {
        return $("#checkbox_2").is(':checked');
    }

    function is_show_sugar() {
        return $("#checkbox_3").is(':checked');
    }
    
    function produceItem_date(date) {
        return '<div class="date tiem0">'
                +    '<div class="icon"></div>'
                +    '<span class="date">'+ date + '</span>'
                +'</div>';
    }

    function produceItem_profit(date, tsr_num, is_show, is_dark) {
        var show_str = is_show ? 'show' : '';   //是否显示 时间icon
        var dark_str = is_dark ? 'dark' : '';   //是否深色背景
        return '<div class="item tiem1 container '+ dark_str +'">'
                +    '<div class="des des-title">锁仓收益</div>'
                +    '<div class="des des-date">' + date+'</div>'
                +    '<div class="des des-detail">'
                +        '<div class="detail-1">+' + tsr_num + 'TSR</div>'
                +    '</div>'
                +   '<div class=" ' + show_str + '"></div>'
                +'</div>';
    }
    function produceItem_reward(date, tsr_num, coupon_num, is_show, is_dark) {
        var coupon_str = coupon_num > 0 ? 'coupon' : '';
        var show_str = is_show ? 'show' : '';
        var dark_str = is_dark ? 'dark' : '';
        return '<div class="item item2 container '+ dark_str +'">'
                +    '<div class="des des-title">返佣奖励</div>'
                +    '<div class="des des-date">' + date + '</div>'
                +    '<div class="des des-detail">'
                +        '<div class="detail-1">+' + tsr_num + 'TSR</div>'
                +        '<div class="detail-2 ' + coupon_str + '">+' + coupon_num + '优惠券</div>'
                +   ' </div>'
                +   '<div class=" ' + show_str + '"></div>'
                + '</div>';
    }

    function produceItem_sugar(date, tsr_num, is_show, is_dark) {
        var show_str = is_show ? 'show' : '';
        var dark_str = is_dark ? 'dark' : '';
        return '<div class="item item3 container '+ dark_str +'">'
                +    '<div class="des des-title">糖果</div>'
                +    '<div class="des des-date">' + date+ '</div>'
                +    '<div class="des des-detail container-end">'
                +        '<div class="detail-1">+'+ tsr_num + 'TSR</div>'
                +    '</div>'
                +    '<div class=" ' + show_str + '"></div>'
                + '</div>';
    }

    function produceItem_total(total_tsr, total_coupon) {
        return '<div class="item-total item4 container-end">'
                + '<div class="tsr">+<span class="total-tsr">'+ total_tsr + '</span>TSR</div>'
                + '<div class="blank"></div>'
                + '<div class="total-coupon"><span class="total-coupon">'+ total_coupon + '</span>优惠券'
                + '</div>';
    }

    // 根据数据生成 显示项
    function produceListItem(number) {
        console.log('', number);
        /***************************************************************************************/
        /**
         * 下列数据需根据真实数据 做对应调整。
         *  */
        var date_str_1 = number.day;    //  '11月21日';
        var date_str_2 = number.time;   //  '2018-11-21';
        var profit = number.static_amount === undefined ? 0 : + number.static_amount;    // 500;
        var reward = number.commission_amount === undefined ? 0 : number.commission_amount;
        var coupon = number.coupon_amount === undefined ? 0 : number.coupon_amount;
        var sugar  = number.directly_amount === undefined ? 0 : number.directly_amount;
        var is_show_1 = false;
        var is_dark_1 = false;
        var is_show_2 = false;
        var is_dark_2 = false;
        var is_show_3 = (number.static_status == 0);
        var is_dark_3 = (number.static_status == 0);
        
        /***************************************************************************************/

        var text_0 = produceItem_date(date_str_1);
        var text_1 = is_show_profit() ? produceItem_profit(date_str_2, profit, is_show_1, is_dark_1) : '';
        var text_2 = is_show_reward() ? produceItem_reward(date_str_2, reward, coupon, is_show_2, is_dark_2) : '';
        var text_3 = is_show_sugar() ? produceItem_sugar(date_str_2, sugar, is_show_3, is_dark_3) : '';
        var total_tsr = (is_show_profit() ? profit : 0) + (is_show_reward() ? reward : 0) + (is_show_sugar() ? sugar : 0);
        var total_coupon = is_show_reward() ? coupon : 0;
        var text_4 = produceItem_total(total_tsr, total_coupon);
        var li_str = '<li class="list-item item-container">' + text_0 + text_1 + text_2 + text_3 + text_4 + '</li>'
        var item_str = (!is_show_profit() && !is_show_reward() && !is_show_sugar()) ? '' : li_str;
        
        return item_str;
    }

    function setBtnDate( selectedMonth ) {
        var text = '';
        if ( selectedMonth ) {
            text = selectedMonth.split('-')[0] + '年' + selectedMonth.split('-')[1] + '月';
        } else {
            // 设置为当前时间
            text = startDate.getFullYear() + '年' + (startDate.getMonth() + 1) + '月';
        }
        
        $('#my-date span').html(text);
    }
    
    
    // 选项按钮 样式更新。
    function refreshOptions() {
        if (is_show_profit()) {
            $(".option1").addClass('option-color');
        } else {
            $(".option1").removeClass('option-color');
        }
        if (is_show_reward()) {
            $(".option2").addClass('option-color');
        } else {
            $(".option2").removeClass('option-color');
        }
        if (is_show_sugar()) {
            $(".option3").addClass('option-color');
        } else {
            $(".option3").removeClass('option-color');
        }
    }

    function updateBonusList( monthData ) {
        console.log('origin', monthData);
        // console.log('local: ', localStore);
        if (!monthData) {
            monthData = localStore[curMonth];
        }
        console.log(monthData);
        
        $('#list-content ul').empty();
        
        $('#op-static').text((monthData.price_static == undefined ? 0 : monthData.price_static ) + 'TSR');
        $('#op-commission').text((monthData.price_commission == undefined ? 0 : monthData.price_commission ) + 'TSR');
        $('#op-directly').text((monthData.price_directly == undefined ? 0 : monthData.price_directly) + 'TSR');
        
        if (len <= 0) {
            $('#list-content ul').text('没有本月的数据！');
        } 
        
        var len = monthData.list.length;
        for (var i = 0; i < len; i++ ) {
            var liData = monthData.list[i];
            $('#list-content ul').append(produceListItem(liData));
        }
    }
    
    function init_view() {
        $("#checkbox_1").prop("checked",true);
        $("#checkbox_2").prop("checked",true);
        $("#checkbox_3").prop("checked",true);
        refreshOptions();
        
        setBtnDate( curMonth );
        
        if (isTest) {
            var data = {};
            updateBonusList( data );
        } else {
            getData(curMonth, function(data) {
                updateBonusList( data );
            });
        }
    }
    
    function getUrl(year, month) {
        console.log('year:', year,  '  month: ', month)
        // return baseUrl + '/api/member/get-bonus?year='+ year +'&month='+ month +'&access-token='+ token +'&types=3,4,5,6';
        return 'http://www.dengwenhao.com:727/api/member/get-bonus?year='+ year +'&month='+ month +'&access-token=TAICNrPewvEVWaRHbasvFGOG8Ba9Fq3Y92X6Szo8T3';
    }
    
    // 请求数据， monthStr: '2018-12'
    function getData( monthStr, cb ) {
        console.log('请求数据...');
        var year = monthStr.split('-')[0];
        var month = monthStr.split('-')[1];
        var url = getUrl(year, month);
        $.get(url, function(data, status) {
            /***************************************************************************************/
            console.log(data, status);
            
            if (status == 'success' && data.errcode == 0) {
                var monthData = data.data;
                localStore[monthStr] = monthData;
                if (typeof cb == 'function') {
                    cb(monthData);
                } else {
                    console.log(cb + ' is not a callback function!');
                }
            } else {
                console.error('Get data failed: ' + curMonth);
            }
            
            /***************************************************************************************/
        });
    }
    
    
    
    $('.btn-back').on('click', function() {
        console.log('back button');
    })
    
    // 锁仓收益按钮
    $('#selection').on('click','.option1',function(ev) {
        if (ev.target.id !== 'checkbox_1') {
            if (is_show_profit()) {
                $("#checkbox_1").prop("checked",false);
            } else {
                $("#checkbox_1").prop("checked",true);
            }
        }
        
        console.log('option1');
        refreshOptions();
        
        updateBonusList();
    })

    // 返佣奖励 按钮
    $('#selection').on('click','.option2',function(ev) {
        if (ev.target.id !== 'checkbox_2') {
            if (is_show_reward()) {
                $("#checkbox_2").prop("checked",false);
            } else {
                $("#checkbox_2").prop("checked",true);
            }
        }
        
        console.log('option2');
        refreshOptions();
        
        updateBonusList();
    })

    // 糖果按钮
    $('#selection').on('click','.option3', function(ev) {
        if (ev.target.id !== 'checkbox_3') {
            if (is_show_sugar()) {
                $("#checkbox_3").prop("checked",false);
            } else {
                $("#checkbox_3").prop("checked",true);
            }
        }
        
        console.log('option3');
        refreshOptions();
        
        updateBonusList();
    })

    // $('.btn-time-select').on('click', function(ev) {
    //     console.log('show date picker');
    // })
    var calendar = new datePicker();
    calendar.init({
        'trigger': '#my-date',  /*按钮选择器，用于触发弹出插件*/
        'type': 'ym',         /*模式：date日期；datetime日期时间；time时间；ym年月；*/
        'minDate':'2010-1-1',   /*最小日期*/
        'maxDate':'2100-12-31', /*最大日期*/
        'onSubmit':function(){  /*确认时触发事件*/
            // var theSelectData = calendar.value;
            console.log('selected:', calendar.value);
            // 选择的月与当前相同，则直接返回。
            if (curMonth == calendar.value) return;
            var nowDate = new Date();
            var selectDate = new Date(calendar.value);
            if (selectDate.getTime() <= nowDate.getTime()) {
                curMonth = calendar.value;
            } else {
                // curMonth = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1);
                console.log('选择的时间段超出，默认显示当前月的数据。 ' + curMonth + '  : 2');
            }
            setBtnDate( curMonth );
            
            if (isTest) {
                updateBonusList({});
                return;
            }
            
            if (localStore[curMonth]) {     // 如果本地缓存有当前月的数据。
                updateBonusList(localStore[curMonth]);
            } else {                        // 否则，重新请求数据。
                getData(curMonth, function(data) {
                    updateBonusList(data);
                });
            }
        },
        'onClose':function(){/*取消时触发事件*/
        }
    });
});